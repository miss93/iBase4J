package org.ibase4j.provider;

import org.ibase4j.core.base.BaseProvider;
import org.ibase4j.model.generator.SysDept;

public interface SysDeptProvider extends BaseProvider<SysDept> {
	
}
